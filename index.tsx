import { Children, component, React, Style } from 'local/react/component'

export const defaultBadgeStyle: Style = {
  display: 'flex',
  padding: '0.25em 0.5em',
  backgroundColor: 'rgba(0, 0, 0, 0.25)',
  borderRadius: '0.25em',
  boxSizing: 'border-box',
  fontWeight: 'bold',
  fontSize: '80%',
  margin: '0 0.5em 1px 0',
  letterSpacing: '0.5px',
  alignItems: 'center',
  opacity: 0.85
}

export const Badge = component.children
  .props<{
    className?: string
    children: Children
    style?: Style
  }>({
    style: defaultBadgeStyle
  })
  .render(({ className, children, style }) => (
    <div style={style} className={className}>
      <span>{children}</span>
    </div>
  ))
